build:
	go build .

install: uninstall build
	@install -v -d "/usr/share/zsh/site-functions/"
	if [ -d "/usr/share/zsh/site-functions" ]; then \
		install -m 0644 -v completions/nw.zsh-completion /usr/share/zsh/site-functions/_nw; \
	fi
	if [ -d "/usr/share/zsh/vendor-completions" ]; then \
		install -m 0644 -v completions/nw.zsh-completion /usr/share/zsh/vendor-completions/_nw; \
	fi
	@install -v -d "/opt/neural_wiki/"
	@cp -r neural-wiki /opt/neural_wiki/nw
	@ln -sf /opt/neural_wiki/nw "/usr/bin/nw"

uninstall:
	@rm -rf /opt/neural_wiki/
	@rm -f /usr/share/zsh/site-functions/_nw
	@rm -f /usr/share/zsh/vendor-completion/_nw

test:
	go test ./...

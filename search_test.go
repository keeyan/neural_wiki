package main

import (
	"slices"
	"testing"
)

func TestFilesWithLinkToAllGivenFilesAllMatching(t *testing.T) {
	otherFiles := []string{"file2", "file3", "file4"}
	LinkFileWithOtherFiles("file1", otherFiles)
	matches := FilesWithLinkToAllGivenFiles([]string{"file2", "file3"})
	if !slices.Contains(matches, "file1") {
		t.Fatalf("Expected search for file2 and file3 to return file1")
	}
}

func TestFilesWithLinkToAllGivenFilesOneNotMatching(t *testing.T) {
	otherFiles := []string{"file2", "file3", "file4"}
	LinkFileWithOtherFiles("file1", otherFiles)
	matches := FilesWithLinkToAllGivenFiles([]string{"file3", "file5"})
	if slices.Contains(matches, "file1") {
		t.Fatalf("Expected search for file3 and file5 not to return file1")
	}
}

package main

import (
	"encoding/json"
	"io"
	"os"
	"testing"
)

func TestDataLoadsFromDatabaseFile(t *testing.T) {
	database = nil
	os.Remove(AbsolutePath(dataFile))
	newDatabase := Database{
		Links:  map[string][]string{"fairphone": {"sustainable"}},
		Config: map[string]string{"file_extension": "md"},
	}
	jsonString, _ := json.MarshalIndent(newDatabase, "", "  ")
	os.WriteFile(AbsolutePath(dataFile), jsonString, os.ModePerm)
	defer os.Remove(AbsolutePath(dataFile))
	data := Data()
	if data.Links["fairphone"][0] != "sustainable" {
		t.Fatalf("Database did not load links from data.json")
	}
	if data.Config["file_extension"] != "md" {
		t.Fatalf("Database did not load config from data.json")
	}
}

func TestDataWithExistingDatabaseLoaded(t *testing.T) {
	database = nil
	os.Remove(AbsolutePath("data.json"))
	LinkFileWithOtherFiles("file1", []string{"file2"})
	Data()
	newDatabase := Database{Links: map[string][]string{}}
	jsonString, _ := json.MarshalIndent(newDatabase, "", "  ")
	os.WriteFile(AbsolutePath(dataFile), jsonString, os.ModePerm)
	data := Data()
	if data.Links["file1"][0] != "file2" {
		t.Fatalf("Database was loaded a second time from the file")
	}
}

func TestWrite(t *testing.T) {
	database = nil
	os.Remove(AbsolutePath("data.json"))
	LinkFileWithOtherFiles("file1", []string{"file2"})
	Write()
	databaseFile := "test_wiki/data.json"
	jsonFile, err := os.Open(databaseFile)
	if err != nil {
		t.Fatalf("Error whilst reading databaseFile")
	}
	var database *Database
	byteValue, _ := io.ReadAll(jsonFile)
	json.Unmarshal([]byte(byteValue), &database)
	if database.Links["file1"][0] != "file2" {
		t.Fatalf("Expected file1 to be linked to file2 in stored database")
	}
}

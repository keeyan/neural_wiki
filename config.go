package main

func GetConfig(configName string) (config string, ok bool) {
	database := Data()

	val, ok := database.Config[configName]
	return val, ok
}

func SetConfig(configName string, configValue string) {
	database := Data()
	database.Config[configName] = configValue
}

package main

import (
	"fmt"
	"os"
	"os/exec"
	"strings"
	"testing"
)

type failingFunction func()

// https://stackoverflow.com/a/33404435/5922350
func assertBadExitCode(t *testing.T, failingFunction failingFunction, testName string) {
	if os.Getenv("BE_CRASHER") == "1" {
		failingFunction()
		return
	}
	cmd := exec.Command(os.Args[0], strings.Join([]string{"-test.run=", testName}, ""))
	cmd.Env = append(os.Environ(), "BE_CRASHER=1")
	err := cmd.Run()
	if e, ok := err.(*exec.ExitError); ok && !e.Success() {
		fmt.Print(err)
		return
	}
	t.Fatalf("process ran with err %v, want exit status 1", err)
}

func TestValueFromArgsAtWithCorrectArgs(t *testing.T) {
	value := ValueFromArgsAt(1, []string{"1", "2"}, "num")
	if value != "2" {
		t.Fatalf("Expected value to be 2")
	}
}

func TestValueFromArgsAtWithTooFewArgs(t *testing.T) {
	failingFunction := func() {
		ValueFromArgsAt(2, []string{"1", "2"}, "num")
	}
	assertBadExitCode(t, failingFunction, "TestValueFromArgsAtWithTooFewArgs")
}

func TestFileFromArgs(t *testing.T) {
	value := FileFromArgs([]string{"command", "filename"})
	if value != "filename" {
		t.Fatalf("Expected value to be filename")
	}
}
func TestArgAtPositionInListWhenArgInList(t *testing.T) {
	value := ArgAtPositionInList(0, []string{"thearg", "extraarg"}, []string{"bla", "thearg"})
	if value != "thearg" {
		t.Fatalf("Expected value to be thearg")
	}
}

func TestArgAtPositionInListWhenArgNotInList(t *testing.T) {
	failingFunction := func() {
		ArgAtPositionInList(0, []string{"thearg", "other"}, []string{"one", "two"})
	}
	assertBadExitCode(t, failingFunction, "TestArgAtPositionInListWhenArgNotInList")
}

func TestSearchTerms(t *testing.T) {
	value := SearchTerms([]string{"search", "file1", "file2"})
	if value[0] != "file1" || value[1] != "file2" {
		t.Fatalf("Expected search terms to be file1 and file2")
	}
}

func TestSearchTermsWithTooFewArgs(t *testing.T) {
	failingFunction := func() {
		SearchTerms([]string{"search"})
	}
	assertBadExitCode(t, failingFunction, "TestSearchTermsWithTooFewArgs")
}

func TestAdditionalFilesFromArgs(t *testing.T) {
	value := AdditionalFilesFromArgs([]string{"link", "file1", "file2", "file3"})
	if value[0] != "file2" || value[1] != "file3" {
		t.Fatalf("Expected additional files to be file2 and file3")
	}
}

func TestAdditionalFilesFromArgsWithTooFewArgs(t *testing.T) {
	failingFunction := func() {
		AdditionalFilesFromArgs([]string{"link", "file1"})
	}
	assertBadExitCode(t, failingFunction, "TestAdditionalFilesFromArgsWithTooFewArgs")
}

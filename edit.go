package main

import (
	"fmt"
	"os"
	"os/exec"
)

type Runner interface {
	Run(cmd *exec.Cmd) error
}

type DefaultCommandRunner struct{}

func (r DefaultCommandRunner) Run(cmd *exec.Cmd) error {
	return cmd.Run()
}

func Edit(filename string, runner Runner) {
	cmd := exec.Command(os.Getenv("EDITOR"), NodePath(filename))
	cmd.Stdout = os.Stdout
	cmd.Stdin = os.Stdin
	cmd.Stderr = os.Stderr
	err := runner.Run(cmd)
	if err != nil {
		fmt.Printf("%s", err)
		os.Exit(1)
	}
}

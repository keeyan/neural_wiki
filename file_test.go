package main

import (
	"errors"
	"os"
	"slices"
	"testing"
)

func TestCreate(t *testing.T) {
	os.Remove(NodePath("createtest"))
	defer os.Remove(NodePath("createtest"))
	Create("createtest")
	if _, err := os.Stat(NodePath("createtest")); errors.Is(err, os.ErrNotExist) {
		t.Fatalf("Expected createtest to exist")
	}
}

func TestDelete(t *testing.T) {
	os.Create(NodePath("deletetest"))
	Delete("deletetest")
	if _, err := os.Stat(NodePath("deletetest")); err == nil {
		t.Fatalf("Expected deletetest not to exist")
	}
}

func TestRenameRenamesTheFile(t *testing.T) {
	os.Create(NodePath("renametest"))
	defer os.Remove(NodePath("newrenametest"))
	Rename("renametest", "newrenametest")
	if _, err := os.Stat(NodePath("renametest")); err == nil {
		t.Fatalf("Expected renametest not to exist")
	}
	if _, err := os.Stat(NodePath("newrenametest")); errors.Is(err, os.ErrNotExist) {
		t.Fatalf("Expected newrenametest to exist")
	}
}

func TestRenameChangesTheLinks(t *testing.T) {
	os.Create(NodePath("renametest"))
	defer os.Remove(NodePath("newrenametest"))
	LinkFileWithOtherFiles("renametest", []string{"renametestlink1", "renametestlink2"})
	Rename("renametest", "newrenametest")
	database := Data()

	if database.Links["renametest"] != nil {
		t.Fatalf("Expected renametest links not to anymore")
	}
	if !slices.Contains(database.Links["newrenametest"], "renametestlink1") {
		t.Fatalf("Expected newrenametest to link to renametestlink1")
	}
	if !slices.Contains(database.Links["newrenametest"], "renametestlink2") {
		t.Fatalf("Expected newrenametest to link to renametestlink2")
	}
	if !slices.Contains(database.Links["renametestlink1"], "newrenametest") {
		t.Fatalf("Expected renametestlink1 to link to newrenametest")
	}
	if slices.Contains(database.Links["renametestlink1"], "renametest") {
		t.Fatalf("Expected renametestlink1 to no longer link to renametest")
	}
}

func TestNodePathWithoutExtension(t *testing.T) {
	nodePath := NodePath("thefile")
	if nodePath != "test_wiki/thefile" {
		t.Fatalf("Expected NodePath to return 'test_wiki/thefile', returned '%s'", nodePath)
	}
}

func TestNodePathWithExtension(t *testing.T) {
	SetConfig("file_extension", "md")
	nodePath := NodePath("thefile")
	if nodePath != "test_wiki/thefile.md" {
		t.Fatalf("Expected NodePath to return 'test_wiki/thefile.md', returned '%s'", nodePath)
	}
}

func TestAbsolutePath(t *testing.T) {
	absolutePath := AbsolutePath("data.json")
	if absolutePath != "test_wiki/data.json" {
		t.Fatalf("Expected NodePath to return 'test_wiki/data.json', returned '%s'", absolutePath)
	}
}

# NeuralWiki

NeuralWiki is yet another notes taking program.

## What distinguishes NeuralWiki?

* It doesn't provide an editor. You can continue to use your own editor as defined by `$EDITOR`.
* All the entries are regular files in your file system allowing you to use whatever tools you're used to (e.g. git).
* Instead of structuring files hierarchically, files are simply linked to each other. Loosely based on how neurons work.
* It doesn't do a lot and stays out of your way. This allows flexibility in usage and lets smarter tools do the heavy lifting.

## Usage

Firstly set the path of your wiki and define an editor. This can be set as an environment variable as such:

```sh
export NEURAL_WIKI_PATH=~/notes/
export EDITOR=vim
```

Create a file:

```sh
nw create sustainable
```

Edit a file using `$EDITOR` (will create the file if it doesn't exist):

```sh
nw edit fairphone
```

Link 2 or more files:

```sh
nw link fairphone sustainable phone
```

Now `fairphone` will be linked to both `sustainable` and `phone`

Search for a file based on its links:

```sh
nw search sustainable phone
```

This will output `fairphone`.

So if you know there's a sustainable phone but can't remember the name, you can just search for it like this.

See more options:

```sh
nw --help
```

## Installation

* Install Go
* Run `sudo make install`

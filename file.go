package main

import (
	"fmt"
	"os"
	"path/filepath"
	"strings"
)

func Create(filename string) {
	_, err := os.Create(NodePath(filename))
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func Delete(filename string) {
	err := os.Remove(NodePath(filename))
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	database := Data()
	links := database.Links[filename]
	UnlinkFileFromOtherFiles(filename, links)
	delete(database.Links, filename)
}

func Rename(originalFilename string, newFilename string) {
	err := os.Rename(NodePath(originalFilename), NodePath(newFilename))
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	database := Data()
	links := database.Links[originalFilename]
	LinkFileWithOtherFiles(newFilename, links)
	UnlinkFileFromOtherFiles(originalFilename, links)
	delete(database.Links, originalFilename)
}

func NodePath(filename string) string {
	fileExtension, ok := GetConfig("file_extension")
	if ok {
		return filepath.Join(wikiPath, strings.Join([]string{filename, fileExtension}, "."))
	} else {
		return filepath.Join(wikiPath, filename)
	}
}

func AbsolutePath(filename string) string {
	return filepath.Join(wikiPath, filename)
}

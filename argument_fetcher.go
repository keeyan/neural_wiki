package main

import (
	"fmt"
	"os"
	"slices"
	"strings"
)

func ValueFromArgsAt(position int, args []string, nameOfValue string) string {
	if len(args) < position+1 {
		fmt.Printf("Must supply a %s\n", nameOfValue)
		os.Exit(1)
	}

	return args[position]
}

func FileFromArgs(args []string) string {
	return ValueFromArgsAt(1, args, "file")
}

func ArgAtPositionInList(position int, args []string, allowedOptions []string) string {
	if len(args) < position+1 {
		fmt.Printf("Must supply one of %v\n", allowedOptions)
		os.Exit(1)
	}

	value := args[position]
	if !slices.Contains(allowedOptions, value) {
		fmt.Printf(
			"Argument %d, must be one of %s\n",
			position+1, strings.Join(allowedOptions, ", "),
		)
		os.Exit(1)
	}

	return value
}

func SearchTerms(args []string) []string {
	if len(args) < 2 {
		fmt.Println("Must supply at least 1 search term")
		os.Exit(1)
	}

	return args[1:]
}

func AdditionalFilesFromArgs(args []string) []string {
	if len(args) < 3 {
		fmt.Println("Must supply at least 1 item to link")
		os.Exit(1)
	}

	return args[2:]
}

package main

import (
	"slices"
	"testing"
)

func TestLinkFileWithOtherFiles(t *testing.T) {
	otherFiles := []string{"file2", "file3"}
	LinkFileWithOtherFiles("file1", otherFiles)
	database := Data()

	if !slices.Contains(database.Links["file2"], "file1") {
		t.Fatalf("Expected file2 to link to file1")
	}
	if !slices.Contains(database.Links["file3"], "file1") {
		t.Fatalf("Expected file3 to link to file1")
	}
	if !slices.Contains(database.Links["file1"], "file2") {
		t.Fatalf("Expected file1 to link to file2")
	}
	if !slices.Contains(database.Links["file1"], "file3") {
		t.Fatalf("Expected file1 to link to file3")
	}
}

func TestUnlinkFileFromOtherFiles(t *testing.T) {
	otherFiles := []string{"file2", "file3", "file4"}
	LinkFileWithOtherFiles("file1", otherFiles)
	UnlinkFileFromOtherFiles("file1", []string{"file3", "file4"})
	database := Data()

	if slices.Contains(database.Links["file1"], "file3") {
		t.Fatalf("Expected file1 not to link to file3")
	}
	if slices.Contains(database.Links["file1"], "file4") {
		t.Fatalf("Expected file1 not to link to file4")
	}
	if slices.Contains(database.Links["file4"], "file1") {
		t.Fatalf("Expected file4 not to link to file1")
	}
	if !slices.Contains(database.Links["file1"], "file2") {
		t.Fatalf("Expected file1 to link to file2")
	}
	if !slices.Contains(database.Links["file2"], "file1") {
		t.Fatalf("Expected file2 to link to file1")
	}
}

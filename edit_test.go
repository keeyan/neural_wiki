package main

import (
	"os"
	"os/exec"
	"testing"
)

type SpyRunner struct {
	RunFunc func(cmd *exec.Cmd) error
}

func (r SpyRunner) Run(cmd *exec.Cmd) error {
	return r.RunFunc(cmd)
}

func TestEdit(t *testing.T) {
	Create("afile")

	var passedCmd *exec.Cmd
	spyRunner := SpyRunner{
		RunFunc: func(cmd *exec.Cmd) error {
			passedCmd = cmd
			return nil
		},
	}
	os.Setenv("EDITOR", "myeditor")
	Edit("afile", spyRunner)
	if passedCmd.Path != "myeditor" {
		t.Fatalf("Editor was called with %s instead of myeditor", passedCmd.Path)
	}
	if len(passedCmd.Args) != 2 || passedCmd.Args[1] != "test_wiki/afile" {
		t.Fatalf("Editor called with %s instead of test_wiki/afile", passedCmd.Args[1])
	}
	Delete("afile")
}

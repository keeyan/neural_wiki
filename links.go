package main

import "slices"

func LinkFileWithOtherFiles(file string, otherFiles []string) {
	database := Data()

	for _, otherFile := range otherFiles {
		if !slices.Contains(database.Links[file], otherFile) {
			database.Links[file] = append(database.Links[file], otherFile)
		}

		if !slices.Contains(database.Links[otherFile], file) {
			database.Links[otherFile] = append(database.Links[otherFile], file)
		}
	}
}

func UnlinkFileFromOtherFiles(file string, otherFiles []string) {
	database := Data()

	for _, otherFile := range otherFiles {
		database.Links[file] = removeElement(database.Links[file], otherFile)
		database.Links[otherFile] = removeElement(database.Links[otherFile], file)
	}
}

func removeElement(slice []string, val string) []string {
	nextElementInsertIndex := 0
	for _, v := range slice {
		if v != val {
			slice[nextElementInsertIndex] = v
			nextElementInsertIndex++
		}
	}
	return slice[:nextElementInsertIndex]
}

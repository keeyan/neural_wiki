package main

func FilesWithLinkToAllGivenFiles(searchTerms []string) []string {
	database := Data()
	remainingValues := database.Links[searchTerms[0]]
	for _, searchTerm := range searchTerms[1:] {
		remainingValues = intersect(remainingValues, database.Links[searchTerm])
	}
	return remainingValues
}

func intersect(slice1, slice2 []string) []string {
	var intersect []string
	for _, element1 := range slice1 {
		for _, element2 := range slice2 {
			if element1 == element2 {
				intersect = append(intersect, element1)
			}
		}
	}
	return intersect
}

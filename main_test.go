package main

import (
	"os"
	"testing"
)

func TestMain(m *testing.M) {
	wikiPath = "test_wiki/"
	code := m.Run()
	os.Exit(code)
}

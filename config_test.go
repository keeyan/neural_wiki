package main

import "testing"

func TestGetConfigWhenConfigDoesNotExist(t *testing.T) {
	fileExtension, ok := GetConfig("file_extension")
	if !(fileExtension == "") {
		t.Fatalf("Expected fileExtension to be empty")
	}
	if ok {
		t.Fatalf("Expected ok to be false")
	}
}

func TestGetConfigWhenConfigExists(t *testing.T) {
	SetConfig("file_extension", "md")

	fileExtension, ok := GetConfig("file_extension")

	if !(fileExtension == "md") {
		t.Fatalf("Expected file_extension to be set to md")
	}

	if !ok {
		t.Fatalf("Expected ok to be true")
	}
}

func TestSetConfigWhenPassingValidKey(t *testing.T) {
	SetConfig("file_extension", "md")

	fileExtension, _ := GetConfig("file_extension")
	if !(fileExtension == "md") {
		t.Fatalf("Expected file_extension to be set to md")
	}
}

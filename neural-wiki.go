package main

import (
	"flag"
	"fmt"
	"os"
	"strings"
)

const wikiPathUsage = "Path to the wiki uses $NEURAL_WIKI_PATH if not present"
const usageBanner = `Usage nw COMMAND

COMMAND can be one of:
* create FILE (c) to create a file but not edit it.
* edit FILE (e) to edit a file. Will create one if it does not exist yet.
* delete FILE (d) to delete a file.
* link FILE1 FILE2 [FILE3...] (l) to create a link between FILE1 and the other files.
* unlink FILE [FILE3...] (u) to remove a link between FILE1 and the other files.
* search FILE1... (s) search for file with links to/from the given files.
* rename OLDNAME NEWNAME (r) renames a file to a new name and updates all the links.
* config [set|get] key [value] to get or set a config value.`

func usage() {
	fmt.Println(usageBanner)
	flag.PrintDefaults()
	os.Exit(2)
}

var (
	wikiPath string
)

func handleCreate(args []string) {
	filename := FileFromArgs(args)
	Create(filename)
}

func handleEdit(args []string) {
	filename := FileFromArgs(args)
	Edit(filename, DefaultCommandRunner{})
}

func handleDelete(args []string) {
	filename := FileFromArgs(args)
	Delete(filename)
	Write()
}

func handleSearch(args []string) {
	searchTermsResponse := SearchTerms(args)
	files := FilesWithLinkToAllGivenFiles(searchTermsResponse)
	fmt.Println(strings.Join(files, "\n"))
}

func handleLink(args []string) {
	filename := FileFromArgs(args)
	additionalFiles := AdditionalFilesFromArgs(args)
	LinkFileWithOtherFiles(filename, additionalFiles)
	Write()
}

func handleUnlink(args []string) {
	filename := FileFromArgs(args)
	additionalFiles := AdditionalFilesFromArgs(args)
	UnlinkFileFromOtherFiles(filename, additionalFiles)
	Write()
}

func handleRename(args []string) {
	originalFilename := FileFromArgs(args)
	newFilename := ValueFromArgsAt(2, args, "file")
	Rename(originalFilename, newFilename)
	Write()
}

func handleConfig(args []string) {
	switch ArgAtPositionInList(1, args, []string{"get", "set"}) {
	case "get":
		value, ok := GetConfig(ArgAtPositionInList(2, args, []string{"file_extension"}))
		if ok {
			fmt.Println(value)
		} else {
			fmt.Println("Could not set config")
			os.Exit(1)
		}
	case "set":
		SetConfig(
			ArgAtPositionInList(2, args, []string{"file_extension"}),
			ValueFromArgsAt(3, args, "value"),
		)
	}
	Write()
}

func main() {
	flag.StringVar(&wikiPath, "wiki-path", os.Getenv("NEURAL_WIKI_PATH"), wikiPathUsage)
	flag.StringVar(&wikiPath, "p", os.Getenv("NEURAL_WIKI_PATH"), wikiPathUsage)
	flag.Usage = usage
	flag.Parse()

	if wikiPath == "" {
		fmt.Println("Must supply a wiki path. See --help")
		os.Exit(1)
	}

	args := flag.Args()

	if len(args) == 0 {
		fmt.Println("Must supply a command. See --help")
		os.Exit(1)
	}

	command := args[0]

	switch command {
	case "create", "c":
		handleCreate(args)
	case "edit", "e":
		handleEdit(args)
	case "delete", "d":
		handleDelete(args)
	case "search", "s":
		handleSearch(args)
	case "link", "l":
		handleLink(args)
	case "unlink", "u":
		handleUnlink(args)
	case "rename", "r":
		handleRename(args)
	case "config":
		handleConfig(args)
	default:
		fmt.Printf("Command %s not recognised\n", command)
		os.Exit(1)
	}
}

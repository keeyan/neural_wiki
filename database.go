package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"os"
)

const dataFile = "data.json"

var database *Database

type Database struct {
	Links  map[string][]string `json:"links"`
	Config map[string]string   `json:"config"`
}

func Data() *Database {
	if database != nil {
		return database
	}

	databaseFile := AbsolutePath(dataFile)

	jsonFile, err := os.Open(databaseFile)

	if errors.Is(err, os.ErrNotExist) {
		jsonFile = createDatabase(databaseFile)
	} else if err != nil {
		fmt.Println(err)
	}

	defer jsonFile.Close()
	byteValue, _ := io.ReadAll(jsonFile)
	json.Unmarshal([]byte(byteValue), &database)
	return database
}

func Write() {
	jsonString, _ := json.MarshalIndent(database, "", "  ")
	os.WriteFile(AbsolutePath(dataFile), jsonString, os.ModePerm)
}

func createDatabase(databaseFile string) *os.File {
	jsonFile, err := os.Create(databaseFile)

	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	links := make(map[string][]string)
	config := make(map[string]string)
	database := Database{Links: links, Config: config}

	jsonToWrite, _ := json.MarshalIndent(database, "", "  ")
	jsonFile.Write(jsonToWrite)
	jsonFile.Seek(0, io.SeekStart)
	return jsonFile
}
